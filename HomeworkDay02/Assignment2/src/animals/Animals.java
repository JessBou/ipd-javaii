package animals;
abstract class Animal {
	abstract void Sound();
}
class Cats extends Animal {
	void Sound() {
		System.out.println("Meow!");
	}
}
class Dogs extends Animal {
	void Sound() {
		System.out.println("Bark!");
	}
}
class Horses extends Animal {
	void Sound() {
		System.out.println("Neigh!");
	}
}
public class Animals {

	public static void main(String[] args) {
		Dogs dog = new Dogs();
		dog.Sound();
		Cats cat = new Cats();
		cat.Sound();
		Horses horse = new Horses();
		horse.Sound();
	}

}
