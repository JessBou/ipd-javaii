package shapes;
abstract class Shape{
	abstract double getArea(double baseLength, double heightLength);
	abstract double getPerimeter(double baseLength, double heightLength);
}
class Square extends Shape {
	
	double getArea(double baseLength, double heightLength) {
		double area = baseLength * heightLength;
		return area;
	}

	double getPerimeter(double baseLength, double heightLength) {
		double perimeter = baseLength * 2 + heightLength * 2;
		return perimeter;
	}
	
}
class IsoscelesTriangle extends Shape {
	
	double getArea(double baseLength, double heightLength) {
		double area = baseLength * heightLength / 2;
		return area;
	}

	double getPerimeter(double baseLength, double heightLength) {
		double perimeter = baseLength + (heightLength * 2);
		return perimeter;
	}
	
}
public class Shapes {

	public static void main(String[] args) {
		System.out.println("Square:");
		Square square = new Square();
		System.out.println("Area: " + square.getArea(5, 5));
		System.out.println("Perimeter: " + square.getPerimeter(5, 5));
		System.out.println("===================================");
		System.out.println("Isosceles triangle:");
		IsoscelesTriangle isoscelesTriangle = new IsoscelesTriangle();
		System.out.println("Area: " + isoscelesTriangle.getArea(4, 7));
		System.out.println("Perimeter: " + isoscelesTriangle.getPerimeter(4, 7));
	}

}
