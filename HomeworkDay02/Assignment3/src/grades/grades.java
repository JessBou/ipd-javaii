package grades;
abstract class Marks {
	abstract void getPercentage();
}
class A extends Marks {
	double math;
	double english;
	double gym;
	A(double math, double english, double gym) {
		this.math = math;
		this.english = english;
		this.gym = gym;
	}
	public void getPercentage() {
		System.out.println("Student A:");
		System.out.println("Math: " + math + "%");
		System.out.println("English: " + english + "%");
		System.out.println("Gym: " + gym + "%");
	}
}
class B extends Marks {
	double math;
	double english;
	double gym;
	double history;
	B(double math, double english, double gym, double history) {
		this.math = math;
		this.english = english;
		this.gym = gym;
		this.history = history;
	}
	public void getPercentage() {
		System.out.println("Student B:");
		System.out.println("Math: " + math + "%");
		System.out.println("English: " + english + "%");
		System.out.println("Gym: " + gym + "%");
		System.out.println("History: " + history + "%");
	}
}
public class grades {

	public static void main(String[] args) {
		A studentA = new A(98, 78, 62);
		studentA.getPercentage();
		B studentB = new B(88, 72, 87, 56);
		studentB.getPercentage();
	}

}
