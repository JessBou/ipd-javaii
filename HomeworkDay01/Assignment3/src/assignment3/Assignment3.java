package assignment3;

import java.util.Scanner;
class Wall {
	static double width;
	static double height;
	
	public Wall() {
		
	}
	public Wall(double widthInput, double heightInput) {
		
	}
	public static double getWidth() {
		return width;
	}
	public static double getHeight() {
		return height;
	}
	public static void setWidth(double widthInput) {
		if (widthInput < 0) {
			width = 0;
		} else {
			width = widthInput;
		}
	}
	public static void setHeight(double heightInput) {
		if (heightInput < 0) {
			height = 0;
		} else {
			height = heightInput;
		}
	}
	public static double getArea() {
		double area = width * height;
		return area;
	}
}
public class Assignment3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Enter width: ");
		double widthInput = input.nextDouble();
		System.out.print("Enter height: ");
		double heightInput = input.nextDouble();
		Wall wall = new Wall(widthInput, heightInput);
		wall.setWidth(widthInput);
		wall.setHeight(heightInput);
		System.out.println("Width: " + wall.getWidth());
		System.out.println("Height: " + wall.getHeight());
		System.out.println("Area: " + wall.getArea());
	}

}
