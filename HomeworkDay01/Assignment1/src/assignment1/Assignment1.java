package assignment1;

import java.util.Scanner;

class Person {
	static Scanner input = new Scanner(System.in);
	static String firstName;
	static String lastName;
	static int age;
	
	public static String getFirstName() {
		System.out.print("What is you're first name: ");
		firstName = input.nextLine();
		return firstName;
	}
	public static String getLastName() {
		System.out.print("What is your last name: ");
		lastName = input.nextLine();
		return lastName;
	}
	public static int getAge() {
		System.out.print("What is your age: ");
		age = input.nextInt();
		return age;
	}
	public static void setFirstName(String first) {
		firstName = first;
	}
	public static void setLastName(String last) {
		lastName = last;
	}
	public static void setAge(int age) {
		if (age < 0 || age > 100) {
			System.out.println("Age: 0");
		}
	}
	public static boolean isTeen() {
		if (age > 12 && age < 20) {
			return true;
		}
		return false;
	}
	public static void getFullname() {
		if (firstName.isEmpty() && lastName.isEmpty()) {
			System.out.println("");
		} else if (firstName.isEmpty()) {
			System.out.println(lastName);
		} else if (lastName.isEmpty()) {
			System.out.println(firstName);
		} else {
			System.out.println(firstName + " " + lastName);
		}
	}
}
public class Assignment1 {

	public static void main(String[] args) {
		Person person = new Person();
		person.getFirstName();
		person.getLastName();
		person.getAge();
		person.getFullname();
		System.out.println("teen= " + person.isTeen());
	}

}
