package assignment2;

import java.util.Scanner;

class SimpleCalculator {
	static Scanner input = new Scanner(System.in);
	static double firstNumber;
	static double secondNumber;
	public static double getFirstNumber() {
		System.out.print("Enter first number: ");
		firstNumber = input.nextDouble();
		return firstNumber;
	}
	public static double getSecondNumber() {
		System.out.print("Enter second number: ");
		secondNumber = input.nextDouble();
		return secondNumber;
	}
	public static void setFirstNumber(double first) {
		firstNumber = first;
	}
	public static void setSecondNumber(double second) {
		secondNumber = second;
	}
	public static double getAdditionResult() {
		double sum = firstNumber + secondNumber;
		return sum;
	}
	public static double getSubtractionResult(){
		double difference = firstNumber - secondNumber;
		return difference;
	}
	public static double getMultiplicationResult() {
		double product = firstNumber * secondNumber;
		return product;
	}
	public static double getDivisonResult() {
		double quotient = firstNumber / secondNumber;
		return quotient;
	}
}
public class Assignment2 {

	public static void main(String[] args) {
		SimpleCalculator calculator = new SimpleCalculator();
		calculator.getFirstNumber();
		calculator.getSecondNumber();
		System.out.println("Sum: " + calculator.getAdditionResult());
		System.out.println("Difference: " + calculator.getSubtractionResult());
		System.out.println("Product: " + calculator.getMultiplicationResult());
		System.out.println("Quotient: " + calculator.getDivisonResult());

	}

}
