package employeesalaries;

public class HourlyEmployee extends Employee {

	protected double wage;
	protected double hours;
	
	public HourlyEmployee(String name, String ssn, double wage, double hours) {
		super(name, ssn);
		this.wage = wage;
		this.hours = hours;
	}

	public double getWage() {
		return wage;
	}
	public double getHours() {
		return hours;
	}
	
	@Override
	public double salary() {
		return getWage() * getHours();
	}

	@Override
	public String toString() {
		return super.toString() + "HourlyEmployee [wage=" + wage + ", hours=" + hours + "]";
	}
	

}
