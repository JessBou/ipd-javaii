package employeesalaries;

public class CommissionEmployee extends Employee {

	protected double sales;
	protected double commission;
	
	public CommissionEmployee(String name, String ssn, double sales, double commission) {
		super(name, ssn);
		this.sales = sales;
		this.commission = commission;
	}
	
	public double getCommission() {
		return commission;
	}
	public double getSales() {
		return sales;
	}
	
	@Override
	public double salary() {
		return getCommission() * getSales();
	}

	@Override
	public String toString() {
		return super.toString() + "CommissionEmployee [sales=" + sales + ", commission=" + commission + "]";
	}
	

}
