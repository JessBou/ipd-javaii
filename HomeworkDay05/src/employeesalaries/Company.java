package employeesalaries;

import java.util.ArrayList;

public class Company {

	public static void main(String[] args) {
		
		ArrayList<Employee> employeeList = new ArrayList<>();
		
		employeeList.add(new HourlyEmployee("Shepherd", "1234", 18.50, 6));
		employeeList.add(new HourlyEmployee("Garrus", "2345", 14.75, 9));
		employeeList.add(new CommissionEmployee("Rex", "3456", 231.14, 5));
		employeeList.add(new CommissionEmployee("Liara", "4567", 116.48, 5));
		employeeList.add(new SalariedEmployee("Miranda", "5678", 28000));
		employeeList.add(new SalariedEmployee("Tali", "6789", 32000));
		
		Payroll payroll = new Payroll(employeeList);
		payroll.paySalary();
	}

}
