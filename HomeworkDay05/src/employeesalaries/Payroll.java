package employeesalaries;

import java.util.ArrayList;

public class Payroll {
	protected ArrayList <Employee> employees = new ArrayList<>();
	
	public Payroll(ArrayList<Employee> employeeList) {
		this.employees = employeeList;
	}
	
	public void paySalary() {
		
		for(Employee emp : employees) {
			System.out.println("The salary for " + emp + " is = " + emp.salary());
		}

	}
}
