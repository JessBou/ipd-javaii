package employeesalaries;

public class SalariedEmployee extends Employee {
	
	protected double basicSalary;

	public SalariedEmployee(String name, String ssn, double basicSalary) {
		super(name, ssn);
		this.basicSalary = basicSalary;
	}
	
	
	public double getBasicSalary() {
		return basicSalary;
	}


	@Override
	public double salary() {
		return getBasicSalary();
	}

	@Override
	public String toString() {
		return super.toString() + "SalariedEmployee [basicSalary=" + basicSalary + "]";
	}
	

}
