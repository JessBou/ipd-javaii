package dancer;

public class Dancer {
	private String name;
	private int age;
	Dancer(String name, int age) {
		this.name = name;
		this.age = age;
	}
	String getName() {
		return name;
	}
	int getAge() {
		return age;
	}
	public String dance() {
		String danceType = "*Awkwardly shuffles around the dance floor*";
		return danceType;
	}
	
	
}
