package dancer;

public class Breakdancer extends Dancer {

	Breakdancer(String name, int age) {
		super(name, age);
		
	}

	@Override
	public String dance() {
		String danceType = "Check out deez moves, yo! *Spins on head*";
		return danceType;
	}
}
