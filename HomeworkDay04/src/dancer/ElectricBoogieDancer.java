package dancer;

public class ElectricBoogieDancer extends Dancer {
	
	ElectricBoogieDancer(String name, int age) {
		super(name, age);
		
	}

	@Override
	public String dance() {
		String danceType = "*Puts on inappropriately loud Electronic music* Let's Boogie-Woogie!";
		return danceType;
	}
}
