package dancer;

public class DancerApp {

	public static void main(String[] args) {
		Dancer[] dancers = new Dancer[]
				{new Dancer("Juji", 41), new ElectricBoogieDancer("Pampa", 17), new Breakdancer("Koko", 26)};
		for(int i = 0; i < dancers.length; i++) {
			EnterDanceCompetition(dancers[i]);
		}
		
	}
	
	public static void EnterDanceCompetition(Dancer nextDancer) {
		
		System.out.println("-Name: " + nextDancer.getName() + "   Age: " + nextDancer.getAge() + 
				"   Come on stage! " + nextDancer.dance());
		
	}

}
