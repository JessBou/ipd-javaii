package interfaceAgain;

public class Player extends Team implements PlayerActions, Stats {

	@Override
	public void playerShoots() {
		System.out.println("Player shoots!");
		
	}

	@Override
	public void playerPasses() {
		System.out.println("Player passes!");
		
	}

	@Override
	public void rank() {
		System.out.println("Top");
		
	}

	@Override
	public void position() {
		System.out.println("Forward");
		
	}

}
