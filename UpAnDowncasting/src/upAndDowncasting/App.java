package upAndDowncasting;

public class App {

	public static void main(String[] args) {
		Machine machine1 = new Machine();
		Camera camera1 = new Camera();
		
		machine1.start();
		camera1.start();
		camera1.snap();
		
		//Upcasting (Safe)
		Machine machine2 = camera1;
		machine2.start();
		//error: machine2.snap(); (snap belongs to the child class "Camera" so it cna't be called in upcasting"
		
		//Downcasting (Unsafe, make sure you do it properly! Object needs to refer to the proper object)
		Machine machine3 = new Camera();
		Camera camera2 = (Camera)machine3;
		camera2.start();
		camera2.snap();
		
		//These will give me a runtime exception:
//		Machine machine4 = new Machine();
//		Camera camera3 = (Camera)machine4;
//		camera3.start();
//		camera3.snap();
		
		Machine machine4 = new Machine();
		machine4.setColor("blue");
		Machine machine5 = machine4;
		machine4.setColor("red");
		System.out.println(machine4);
		System.out.println(machine5);

	}

}
