package tasteTesting;

import static org.junit.Assert.*;

import org.junit.Test;

public class squareTest {

	@Test
	public void test() {
		TasteTester test = new TasteTester();
		int output = test.square(5);
		assertEquals(25, output);
	}

}
