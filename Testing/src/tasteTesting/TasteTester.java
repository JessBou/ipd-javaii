package tasteTesting;

public class TasteTester {
	
	public int square(int x) {
		return x * x;
	}
	
	public int countA(String yumyum) {
		int count = 0;
		for(int i = 0; i < yumyum.length(); i++) {
			if(yumyum.charAt(i) == 'a' || yumyum.charAt(i) == 'A') {
				count++;
			}
		}
		return count;
	}

}
