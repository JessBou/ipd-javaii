package swimmingpool;
class Rectangle {
	double width;
	double length;
	Rectangle(double width, double length) {
		this.width = width;
		this.length = length;
		if(width < 0) {
			this.width = 0;
		}
		if(length < 0) {
			this.length = 0;
		}
	}
	public double getWidth() {
		return width;
	}
	public double getLength() {
		return length;
	}
	public double getArea() {
		double area = width * length;
		return area;
	}
}
class Cuboid extends Rectangle {
	double height;
	Cuboid(double width, double length, double height) {
		super(width, length);
		this.height = height;
		if(height < 0) {
			this.height = 0;
		}
	}
	public double getHeight() {
		return height;
	}
	public double getVolume() {
		double volume = width * length * height;
		return volume;
	}
}
public class Pool {

	public static void main(String[] args) {
		Rectangle rectangle = new Rectangle(5, 10);
		System.out.println("Rectangle width is: " + rectangle.getWidth());
		System.out.println("Rectangle length is: " + rectangle.getLength());
		System.out.println("Recangle area is: " + rectangle.getArea());
		System.out.println("========================================");
		Cuboid cuboid = new Cuboid(5, 10, 5);
		System.out.println("Cuboid width is: " + cuboid.getWidth());
		System.out.println("Cuboid length is: " + cuboid.getLength());
		System.out.println("Cuboid area is: " + cuboid.getArea());
		System.out.println("Cuboid height is: " + cuboid.getHeight());
		System.out.println("Cuboid volume is: " + cuboid.getVolume());

	}

}
