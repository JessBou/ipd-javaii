package sendingMessages;

public class SmsUser extends User{
	
	String phoneNumber;

	public SmsUser(String firstName, String lastName, Address address, String phoneNumber) {
		super(firstName, lastName, address);
		setPhoneNumber(phoneNumber);
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) throws IllegalArgumentException {
		 try {
	            if (phoneNumber.matches("[0-9]+")) {
	                this.phoneNumber = phoneNumber;
	        }
	      }
	        catch(IllegalArgumentException e) {
	            System.out.println("Must enter digits between 0-9");
	        }
		 if (phoneNumber.length() < 10) {
			 throw new IllegalArgumentException("Must enter 10 digits for phone number");
		 }
		 
	}
	
	

}
