package sendingMessages;

public class Message {
	
	private User receiver;
	private User sender;
	private String body;
	public Message(User receiver, User sender, String body) {
		setReceiver(receiver);
		setSender(sender);
		setBody(body);
	}
	public User getReceiver() {
		return receiver;
	}
	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}
	public User getSender() {
		return sender;
	}
	public void setSender(User sender) {
		this.sender = sender;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		if (body.isEmpty() || body.contains("^") || body.contains("*") || body.contains("!")) {
            throw new IllegalArgumentException("Must not be empty \n Must not containt characters: '^', '*', or '!'");
        } else {
            this.body = body;
        }
	}
	@Override
	public String toString() {
		return "Message [receiver=" + receiver + ", sender=" + sender + ", body=" + body + "] \n";
	}
	
	

}
