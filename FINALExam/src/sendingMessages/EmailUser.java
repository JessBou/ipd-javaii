package sendingMessages;

public class EmailUser extends User{
	
	private String emailAddress;

	public EmailUser(String firstName, String lastName, Address address, String emailAddress) {
		super(firstName, lastName, address);
		setEmailAddress(emailAddress);
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		if (emailAddress.contains("@") && emailAddress.contains(".")) {
            this.emailAddress = emailAddress;
        } else {
        	throw new IllegalArgumentException("needs '@' symbol and '.'");
        }
	}
	

}
