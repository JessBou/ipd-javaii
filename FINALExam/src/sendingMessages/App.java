package sendingMessages;

import java.util.ArrayList;
import java.util.List;

public class App {

	public static void main(String[] args) {
		
		List<Message> listOfMessages = new ArrayList<Message>();
		
		
		EmailMessage email = new EmailMessage(
                new EmailUser("Judy", "Foster", new Address("Main Street", 1), "a.d@g.com"),
                new EmailUser("Betty", "Beans", new Address("second street", 2), "v.r@g.com"),
                "This is one email");
		SmsMessage smsMessage = new SmsMessage(
				new SmsUser("Jeff", "Winger", new Address("First Street", 3), "1234567890"),
				new SmsUser("Dean", "Pelton", new Address("Second Street", 4), "0987654321"),
				"This is one sms");
		
			listOfMessages.add(email);
			listOfMessages.add(smsMessage);
			
			//This next part... I don't think it's the proper thing, but it's the only way I got it to write!			
			SendEmail e = new SendEmail();
			e.sendMessage(email);
			
			SendSms s = new SendSms();
			s.sendMessage(smsMessage);
			
			
		

	}

}
