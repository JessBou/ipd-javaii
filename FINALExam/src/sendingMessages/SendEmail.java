package sendingMessages;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class SendEmail implements ISendInfo{

	@Override
	public boolean validateMessage(User sender, User receiver, String body) {
		
		//I struggle a lot with validating and exceptions still... I need more practice! :(
		//Not sure how to validate using this method... I validated in other methods instead!
		Pattern pattern = Pattern.compile(sender.toString());
	    Matcher matcher = pattern.matcher("@");
	    boolean matchFound = matcher.find();
	    if(matchFound != matcher.find()) {
	    	throw new IllegalArgumentException("needs '@' symbol.");
	    }
	    return true;
		
		
	}

	@Override
	public void sendMessage(Message message) {
		
		File email = new File("src/sendingMessages/email.txt");
		
		try {
		      
		      if (email.createNewFile()) {
		        System.out.println("File created: " + email.getName());
		      } else {
		        System.out.println("File already exists.");
		      }
		    } catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
		
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(email.getAbsolutePath())))  {
                 writer.write(message.toString() + "\n");
			
		} catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
		
	}

}
