package sendingMessages;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class SendSms implements ISendInfo{
	
	@Override
	public boolean validateMessage(User sender, User receiver, String body) {
		return true;
	}

	@Override
	public void sendMessage(Message message) {
		File sms = new File("src/sendingMessages/sms.txt");
		
		try {
		      
		      if (sms.createNewFile()) {
		        System.out.println("File created: " + sms.getName());
		      } else {
		        System.out.println("File already exists.");
		      }
		    } catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
		
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(sms.getAbsolutePath())))  {
                 writer.write(message.toString() + "\n");
			
		} catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
		
	}

}
