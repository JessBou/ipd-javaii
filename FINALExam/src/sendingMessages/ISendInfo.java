package sendingMessages;

public interface ISendInfo {
	
	boolean validateMessage(User sender, User receiver, String body);
	void sendMessage(Message message);

}
