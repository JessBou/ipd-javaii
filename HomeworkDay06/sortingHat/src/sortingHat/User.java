package sortingHat;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class User {
	int id;
	String name;
	java.time.LocalDate birthdate;
	
	User(int id, String name, java.time.LocalDate birthdate) {
		this.id = id;
		this.name = name;
		this.birthdate = birthdate;
	}
	
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public java.time.LocalDate getBirthdate() {
		return birthdate;
	}
	
	ArrayList<User> users = new ArrayList<>();
	public User() {
		users.add(new User (32, "Neville Longbottom", LocalDate.parse("1986-04-13")));
		users.add(new User (38, "Luna Lovegood", LocalDate.parse("1992-05-28")));
		users.add(new User (04, "Remus Lupin", LocalDate.parse("1967-10-31")));
		users.add(new User (14, "Gilderoy Lockhart", LocalDate.parse("1969-12-24")));
		users.add(new User (21, "Rubeus Hagrid", LocalDate.parse("1966-07-02")));
	}

	public void sortListById() {
		Collections.sort(users, new Comparator<User>() {

			@Override
			public int compare(User o1, User o2) {
				return Integer.valueOf(o1.getId()).compareTo(o2.getId());
			}
			
		});
		for(int i = 0; i < users.size(); i++) {
			System.out.println("Sorted by ID: " + users.get(i).getId() + "  " + users.get(i).getName()
					+ "  " + users.get(i).getBirthdate());
		}
	}
	
	public void sortListByName() {
		Collections.sort(users, new Comparator<User>() {

			@Override
			public int compare(User o1, User o2) {
				return Integer.valueOf(o1.getName().compareTo(o2.getName()));
			}
			
		});
		for(int i = 0; i < users.size(); i++) {
			System.out.println("Sorted by name: " + users.get(i).getId() + "  " + users.get(i).getName()
					+ "  " + users.get(i).getBirthdate());
		}
	}
	
	public void sortListByBirthdate() {
		Collections.sort(users, new Comparator<User>() {

			@Override
			public int compare(User o1, User o2) {
				return Integer.valueOf(o1.getBirthdate().compareTo(o2.getBirthdate()));
			}
			
		});
		for(int i = 0; i < users.size(); i++) {
			System.out.println("Sorted by birthdate: " + users.get(i).getId() + "  " + users.get(i).getName()
					+ "  " + users.get(i).getBirthdate());
		}
	}

}
