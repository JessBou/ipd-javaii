package sortingHat;


public class SortingHat {

	public static void main(String[] args) {
		
		User sortMe = new User();
		sortMe.sortListById();
		System.out.println("===============================");
		sortMe.sortListByName();
		System.out.println("===============================");
		sortMe.sortListByBirthdate();

	}

}
