package iceCreamShop;

public class TheNiceOne extends Client{

	public TheNiceOne(String name, String hatColor, double price, String flavour) {
		super(name, hatColor, price, flavour);
	}
	
	public void greet() {
		System.out.println("Hallooo! How're yooouuuu");
	}

	@Override
	public void chooseType() {
		
		FlavoursAndPrices iceCream2 = new FlavoursAndPrices(3.49, "Chocolate");
		System.out.println("CHOCOLATE PLEEEEAAASE!!");
		
	}

	@Override
	public void chooseAmount() {
		
		System.out.println("I'd like 2 scoops pleeeease!");
		
	}

	@Override
	public void payBills() {
		
		System.out.println("Here's $5.00!");
		
	}
	
	public void tip() {
		
		System.out.println("Keep the change!");
	}

}
