package iceCreamShop;

import java.util.HashMap;

public class FlavoursAndPrices {
	private double price;
	private String flavour;
	
	public FlavoursAndPrices(double price, String flavour) {
		setPrice(price);
		setFlavour(flavour);
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getFlavour() {
		return flavour;
	}

	public void setFlavour(String flavour) {
		this.flavour = flavour;
	}
	
//	FlavoursAndPrices iceCream1 = new FlavoursAndPrices(2.99, "Vanilla");
//	FlavoursAndPrices iceCream2 = new FlavoursAndPrices(3.49, "Chocolate");
//    FlavoursAndPrices iceCream3 = new FlavoursAndPrices(3.99, "Melon");
//    FlavoursAndPrices iceCream4 = new FlavoursAndPrices(3.99, "Caramel Swirl");
//    FlavoursAndPrices iceCream5 = new FlavoursAndPrices(4.05, "Reeses Pieces");

//	HashMap<Double, String> flavourMap = new HashMap<Double, String>();{
//		flavourMap.put(iceCream1.getPrice(), iceCream1.getFlavour());
//		flavourMap.put(iceCream2.getPrice(), iceCream2.getFlavour());
//		flavourMap.put(iceCream3.getPrice(), iceCream3.getFlavour());
//		flavourMap.put(iceCream4.getPrice(), iceCream4.getFlavour());
//		flavourMap.put(iceCream5.getPrice(), iceCream5.getFlavour());
//	}
	
	

}
