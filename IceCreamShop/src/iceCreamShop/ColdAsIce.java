package iceCreamShop;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class ColdAsIce {
	
	public static void writeReceipt(List<Client> clients) throws IOException {
		//write file
		
		File myObj = new File("src/iceCreamShop/Receipt/receipt.txt");
		try {
		      
		      if (myObj.createNewFile()) {
		        System.out.println("File created: " + myObj.getName());
		      } else {
		        System.out.println("File already exists.");
		      }
		    } catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
		
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(myObj.getAbsolutePath())))  {
			for(Client c : clients) {
				writer.write(c.toString() + "\n");
			}
			
		} catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
	}
	
	public static void giveReceipt() throws IOException {
		//read file
		try(BufferedReader reader = new  BufferedReader(new FileReader("src/iceCreamShop/Receipt/receipt.txt"))){
			Stream<String> lines = reader.lines();
			lines.forEach(line -> System.out.println(line));
		} catch (IOException e) {
			throw e;
		}
		
	}

	public static void main(String[] args) {
		
		List<Client> clients = new ArrayList<Client>();
		
		//Downcasting one of the clients
		Client client1 = new TheNiceOne("Jack", "Blue hat", 3.49, "Chocolate");
		TheNiceOne nice1 = (TheNiceOne)client1;
		clients.add(nice1);
		
		clients.add(new TheNiceOne("Molly", "Yellow hat", 3.49, "Chocolate"));
		clients.add(new ConspiracyTheorist("Benoit", "Aluminum cap", 3.99, "Melon"));
		clients.add(new TheRudeOne("Karen", "Curly wig", 3.99, "Caramel Swirl"));
		clients.add(new OverTalkativeOne("Trinity", "Fur hoody", 3.49, "Vanilla"));
		
		try {
			writeReceipt(clients);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		try {
			giveReceipt();
		}  catch (IOException e) {
			
			e.printStackTrace();
		}

	}

}
