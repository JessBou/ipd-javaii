package iceCreamShop;

public class TheRudeOne extends Client {

	public TheRudeOne(String name, String hatColor, double price, String flavour) {
		super(name, hatColor, price, flavour);
	}
	
	public void noMask() {
		System.out.println("I have a health thing. My doctor says I can't wear a mask. It's my right!");
	}

	@Override
	public void chooseType() {
		System.out.println("What?! You don't have banana flavour??? Ugh, fine, I'll just go with caramel");
		FlavoursAndPrices iceCream4 = new FlavoursAndPrices(3.99, "Caramel Swirl");
	}

	@Override
	public void chooseAmount() {
		
		System.out.println("Ugh, one scoop, I guess... I'd have more if you had banana flavour.");
		
	}
	
	public void complain() {
		
		System.out.println("You know what? This is an outrage! You ADVERTISED that you had 64 flavours, and you don't have the one I wanted! It's FAlSE ADVERTISING!");
		
	}
	
	public void askForManager() {
		System.out.println("Let me speak to the manager. Now.");
		
	}

	@Override
	public void payBills() {
		
		System.out.println("You should pay me for mental health damages. But your manager said I didn't need to pay, so have a good day.");
		
	}

}
