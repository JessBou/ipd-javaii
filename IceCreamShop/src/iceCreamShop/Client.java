package iceCreamShop;

public abstract class Client {
	String name;
	String hatColor;
	double price;
	String flavour;
	public Client(String name, String hatColor, double price, String flavour) {
		setName(name);
		setHatColor(hatColor);
		setPrice(price);
		setFlavour(flavour);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHatColor() {
		return hatColor;
	}
	public void setHatColor(String hatColor) {
		this.hatColor = hatColor;
	}

	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getFlavour() {
		return flavour;
	}
	public void setFlavour(String flavour) {
		this.flavour = flavour;
	}
	public void walksInShop() {
		System.out.println("Walking into Cold as Ice shop");
	}
	
	public abstract void chooseType();
	public abstract void chooseAmount();
	public abstract void payBills();
	@Override
	public String toString() {
		return "Client [name=" + name + ", hatColor=" + hatColor + ", price=" + price + ", flavour=" + flavour + "]";
	}
	
	
	
	

}
