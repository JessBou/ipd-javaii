package iceCreamShop;

import java.io.IOException;
import java.util.ArrayList;

public interface EmployeeTasks {
	void takeOrder();
	void scoopIceCream();
	void calculatePrice();
	void useRegister();

}
