package cheeseburger;
class Burger {
	Bun bun;
	Topping topping;
	Meat meat;
	Cheese cheese;
	Burger(Bun bun, Topping topping, Meat meat, Cheese cheese) {
		this.bun = bun;
		this.topping = topping;
		this.meat = meat;
		this.cheese = cheese;
	}
	void assembleBurger() {
		System.out.println(bun.getBun());
		System.out.println(cheese.getCheese());
		System.out.println(meat.getMeat());
		System.out.println(topping.getTopping());
	}
}
class Bun {
	private String bunType;
	Bun(String bunType) {
		this.bunType = bunType;
	}
	public String getBun() {
		return bunType;
	}
}
class Topping {
	private String topping;
	Topping(String topping) {
		this.topping = topping;
	}
	public String getTopping() {
		return topping;
	}
}
class Meat {
	private String meatType;
	Meat(String meatType) {
		this.meatType = meatType;
	}
	public String getMeat() {
		return meatType;
	}
}
class Cheese {
	private String cheeseType;
	Cheese(String cheeseType) {
		this.cheeseType = cheeseType;
	}
	public String getCheese() {
		return cheeseType;
	}
}
public class Cheeseburger {

	public static void main(String[] args) {
		
		Bun bun = new Bun("Pumpernickel");
		Cheese cheese = new Cheese("Swiss");
		Meat meat = new Meat("Beef");
		Topping topping = new Topping("Tomato");
		Burger cheeseburger = new Burger(bun, topping, meat, cheese);
		cheeseburger.assembleBurger();
	}

}
