package interfacePractice;

interface WaterBottleInterface {
	
	String color = "Blue";
	
	void fillUp();
	void pourOut();
}

public class practiceInterface implements WaterBottleInterface {

	public static void main(String[] args) {
		
		System.out.println(color);
		
		practiceInterface ex = new practiceInterface();
		ex.fillUp();
		ex.pourOut();
		

	}

	@Override
	public void fillUp() {
		System.out.println("It is filled");
		
	}

	@Override
	public void pourOut() {
		System.out.println("Pour out the water");
		
	}

}
