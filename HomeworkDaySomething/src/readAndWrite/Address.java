package readAndWrite;

public class Address {
	
	private int buildingNo;
	private String streetName;
	private String cityName;
	private String provinceName;
	private String postalCode;
	public Address(int buildingNo, String streetName, String cityName, String provinceName, String postalCode) {
	
		setBuildingNo(buildingNo);
		setStreetName(streetName);
		setCityName(cityName);
		setProvinceName(provinceName);
		setPostalCode(postalCode);
	}
	public int getBuildingNo() {
		return buildingNo;
	}
	public void setBuildingNo(int buildingNo) {
		this.buildingNo = buildingNo;
	}
	public String getStreetName() {
		return streetName;
	}
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	@Override
	public String toString() {
		return "Address [buildingNo=" + buildingNo + ", streetName=" + streetName + ", cityName=" + cityName
				+ ", provinceName=" + provinceName + ", postalCode=" + postalCode + "]";
	}
	
	
	
	

}
