package readAndWrite;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class ReaderWriter {
	
	public static void writeToFile(ArrayList<Address> addresses) throws IOException {
		
		File myObj = new File("src/readAndWrite/Resource/famousAddresses.txt");
		
		try {
		      
		      if (myObj.createNewFile()) {
		        System.out.println("File created: " + myObj.getName());
		      } else {
		        System.out.println("File already exists.");
		      }
		    } catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
		
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(myObj.getAbsolutePath())))  {
			for(Address a : addresses) {
				writer.write("Addresses: " + a.getBuildingNo() + " " + a.getStreetName() + " " + a.getCityName() + " " + a.getProvinceName() + " " + a.getPostalCode() + "\n");
			}
		} catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
	}
	
	
	public static void readFromFile() throws IOException {
		
			try(BufferedReader reader = new  BufferedReader(new FileReader("src/readAndWrite/Resource/famousAddresses.txt"))){
				Stream<String> lines = reader.lines();
				lines.forEach(line -> System.out.println(line));
			} catch (IOException e) {
				throw e;
			}
	}

	public static void main(String[] args) {
		
		ArrayList<Address> addresses = new ArrayList<Address>();
		addresses.add(new Address(221, "Baker Street", "London", "Greater London", "NW1 6XE"));
		addresses.add(new Address(805, "St Cloud Road", "Bel Air", "California", "103489"));
		addresses.add(new Address(1938, "Sulivan Ln", "Metropolis", "Illinois", "435234"));
		addresses.add(new Address(740, "Evergreen Terrace", "Springfield", "Oregon", "546987"));
		addresses.add(new Address(4, "Privet Drive", "Little Whinging", "Surrey", "PH7 8FE"));
		
		try {
			writeToFile(addresses);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		try {
			readFromFile();
		} catch (IOException e) {
			
			e.printStackTrace();
		}

	}

}
